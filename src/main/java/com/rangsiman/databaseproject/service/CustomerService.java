/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rangsiman.databaseproject.service;

import com.rangsiman.databaseproject.dao.CustomerDao;
import com.rangsiman.databaseproject.model.Customer;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CustomerService {

    public Customer getByTel(String tel) {
        CustomerDao CustomerDao = new CustomerDao();
        Customer Customer = CustomerDao.getByTel(tel);
        return Customer;
    }

    public List<Customer> getCustomers() {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.getAll(" Customer_id asc");
    }

    public Customer addNew(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.delete(editedCustomer);
    }
}
