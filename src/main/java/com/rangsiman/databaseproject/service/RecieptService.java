/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rangsiman.databaseproject.service;

import com.rangsiman.databaseproject.dao.RecieptDao;
import com.rangsiman.databaseproject.dao.RecieptDetailDao;
import com.rangsiman.databaseproject.model.Reciept;
import com.rangsiman.databaseproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptService {

    public List<Reciept> getreciepts() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id asc");
    }

    public Reciept addNew(Reciept editedreciept) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = recieptDao.save(editedreciept);
        for (RecieptDetail rd : editedreciept.getRecieptDetails()) {
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Reciept update(Reciept editedreciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedreciept);
    }

    public int delete(Reciept editedreciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedreciept);
    }
}
